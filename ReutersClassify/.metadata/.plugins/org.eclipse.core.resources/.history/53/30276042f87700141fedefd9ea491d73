package ch.ethz.dal.classifier.processing
import scala.math._
import java.io.PrintWriter
import scala.util.Random


object NaiveBayes {
  def perform(folderpath:String)={
	  	val path = folderpath+"data/train"
	  	val iter = new ReutersCorpusIterator(path)
	    val p=new PorterStemmer
	    val stopper=new StopWords
	    val stemmed= collection.mutable.Map[String,String]()
	    def log2 (x: Double) :Double = log10(x) / log10(2.0)

//learn the probability function
	    val topicCounts = scala.collection.mutable.Map[String, Int]()
	    val numerator = scala.collection.mutable.Map[(String,String), Int]()
		val denominator = scala.collection.mutable.Map[String, Int]()
	  	var count = 0   
		while(iter.hasNext){
	    	val doc = iter.next
	    	val tokenized=stopper.filter(doc.tokens.map(_.toLowerCase))
	    	stemmed++=tokenized.map(s=>s->stemmed.getOrElse(s,p.stem(s)))
	    	topicCounts ++= doc.topics.map(c => (c -> (1 + topicCounts.getOrElse(c, 0))))
	    	count += 1
	    	val input=tokenized.flatMap(x=>stemmed.get(x))
	    	val tf= input.groupBy(identity).mapValues(l => l.length)
	    	doc.topics.foreach(x=> {numerator++=tf.map(c => ((c._1,x) -> (c._2 + numerator.getOrElse((c._1,x), 0))));
	    	denominator += ( x-> (input.size + denominator.getOrElse(x, 0)))})
	    	println(count)
		}
		val Pc=topicCounts.mapValues(_.toDouble/count)
		val s=stemmed.size
		val denominatorSmooth=denominator.mapValues(_+s)
		val pwc=numerator.map(x=>(x._1->(x._2.toDouble+1.0)/(denominatorSmooth.get(x._1._2).get)))
		val topic=topicCounts.keys.toList
		

//learning treshold to decide how many topic must be accepted
		
		val randomGen=new Random()
		var count2=0
		var treshold=0.0
		var iter2 = new ReutersCorpusIterator(path)
		while (count2<count*0.05){//they must be at least 1/20 of the collection...
		  	iter2 = new ReutersCorpusIterator(path)
		  	while(iter2.hasNext){
		  	  val doc = iter2.next
		  	  val random=randomGen.nextDouble()
		  		if(random>0.8){//but on average they will be the 20%
		  			count2+=1
		  			if(doc.topics.size!=0&&doc.tokens.size!=0){
		  				val tokenized=stopper.filter(doc.tokens.map(_.toLowerCase))
		  				val totTopic=doc.topics.size
		  				val input=tokenized.flatMap(x=>stemmed.get(x))
		  				val tf= input.groupBy(identity).mapValues(l => l.length)
		  				println ("random "+count2)
	    	
	    	  
	    	
		  				//rank the topics
		  				val e=topic.map(x=>(x,log(Pc.get(x).get) +
		  						(tf.map{case (w,tef)=>tef*log(pwc.getOrElse((w,x),1.0/denominatorSmooth.get(x).get)).toDouble}.sum))).sortBy(_._2).reverse.zipWithIndex
		  				var maxIndex=0
		  				val emap=e.map{case((a,b),c)=>a->(b,c)}.toMap
		  				var counter=0
		  				//find the index that maximize recall
		  				doc.topics.foreach(x=>{val t=emap.get(x).get._2; if (t>maxIndex) maxIndex=t})
		  				//set the treshold
		  				treshold+=e(maxIndex)._1._2/e(0)._1._2-1.0
		  			}
		
		  		}
		  	}
		}
		//the final treshold is the average
		treshold=treshold/count2
		
//evaluate labelled tests: is exactly as in the learning treshold phase, but now we keep
//the best topics according to the founded treshold
	  	val res=scala.collection.mutable.Buffer[(Int,scala.collection.immutable.Set[String])]()
	  	var avg=0.0
	  	count=0
	  	
	  	var precision=0.0
	  	var recall=0.0
	  	var F1=0.0
	  	val pathTL = folderpath+"data/test-with-labels"
		val iterTL = new ReutersCorpusIterator(pathTL)

		while(iterTL.hasNext){
			count+=1
	    	val doc = iterTL.next
	    	if(doc.topics.size!=0&&doc.tokens.size!=0){
	    	val tokenized=stopper.filter(doc.tokens.map(_.toLowerCase))
	    	stemmed++=tokenized.map(s=>s->stemmed.getOrElse(s,p.stem(s)))
	    	val totTopic=doc.topics.size
	    	val input=tokenized.flatMap(x=>stemmed.get(x))
	    	val tf= input.groupBy(identity).mapValues(l => l.length)
	    	println ("labelled test "+count)
	    	
			val e=topic.map(x=>(x,log(Pc.get(x).get) +
	    	    (tf.map{case (w,tef)=>tef*log(pwc.getOrElse((w,x),1.0/denominatorSmooth.get(x).get)).toDouble}.sum))).sortBy(_._2).reverse
			val efirst=e(0)
			val relevant=e.filter(x=>x._2>(1+treshold)*efirst._2).map(x=>x._1).toSet
	    	res+=((doc.ID,relevant))
	    	
	    	val truePositive=(doc.topics & relevant).size
	    	val falsePositive=(relevant -- doc.topics).size
	    	
	    	val pre=truePositive.toDouble/(relevant.size)
	    	val rec=truePositive.toDouble/totTopic
	    	precision+=pre
	    	recall+=rec
	    	if(pre+rec!=0)
	    		F1+=2*pre*rec/(pre+rec)
	    	}

		}
	  val writer=new PrintWriter(folderpath+"classify-[francesco]-[locatello]-[l]-[nb].run", "UTF-8")

	  writer.println(precision/count+" "+recall/count+" "+F1/count)
	 
	 res.foreach(x=>writer.println(x._1+" "+x._2.mkString(" ")))
	  writer.close()
	  
	  
	//test unlabelled data  
	  	val pathTU = folderpath+"data/test-without-labels"
	  	println (pathTU)
		val iterTU = new ReutersCorpusIterator(pathTU)
	  	val resU=scala.collection.mutable.Buffer[(Int,scala.collection.immutable.Set[String])]()
	  	var unlabelled=0
		while(iterTU.hasNext){
			unlabelled+=1
			println("unlabelled: "+unlabelled)
	    	val doc = iterTU.next
	    	if(doc.topics.size!=0&&doc.tokens.size!=0){
	    	val tokenized=stopper.filter(doc.tokens.map(_.toLowerCase))
	    	stemmed++=tokenized.map(s=>s->stemmed.getOrElse(s,p.stem(s)))
	    	//val totTopic=doc.topics.size
	    	val input=tokenized.flatMap(x=>stemmed.get(x))
	    	val tf= input.groupBy(identity).mapValues(l => l.length)
	    	println ("unlabelled test "+count)
	    	
			val e=topic.map(x=>(x,log(Pc.get(x).get) +
	    	    (tf.map{case (w,tef)=>tef*log(pwc.getOrElse((w,x),1.0/denominatorSmooth.get(x).get)).toDouble}.sum))).sortBy(_._2).reverse
			val efirst=e(0)
			val relevant=e.filter(x=>x._2>(1+treshold)*efirst._2).map(x=>x._1).toSet
	    	resU+=((doc.ID,relevant))
	    	println(resU)
	    	
	    	}

		}
	  val writer2=new PrintWriter(folderpath+"classify-[francesco]-[locatello]-[u]-[nb].run", "UTF-8")

	 
	 resU.foreach(x=>writer2.println(x._1+" "+x._2.mkString(" ")))
	  writer2.close()
  }
}