package ch.ethz.dal.classifier.processing
import java.io.File
import java.io.FileNotFoundException
import java.util.zip.ZipEntry
import java.util.zip.ZipFile
import scala.collection.JavaConversions.enumerationAsScalaIterator
import scala.collection.mutable.Queue
import scala.util.Success
import scala.util.Try
import Array._
import scala.math._
import java.io.PrintWriter
import scala.util.Random


 
object LogisticRegression {
//vec: perform dot product between two maps
   def vec (m: scala.collection.Map[String,Double],other: scala.collection.Map[String,Double]) : Double ={
		   m.map{case (k,v) => v*other.getOrElse(k,0.0)}.sum}
//sum: sum two maps 
   def sum(a:scala.collection.mutable.Map[String,Double],b:scala.collection.Map[String,Double]):scala.collection.mutable.Map[String,Double]={ 
		   b.foreach{case (k,v)=> a+= ((k, v+ a.getOrElse(k, 0.0)))}
		   a}
   def log2 (x: Double) :Double = log10(x) / log10(2.0)
//main function: perform logistic regression  
   def logisticRegression(folderpath:String)={   
     
	val path = folderpath+"data/train"
	val iter = new ReutersCorpusIterator(path)
	val firstiter=new ReutersCorpusIterator(path)
	val p=new PorterStemmer
	val stopper=new StopWords
	val stemmed= collection.mutable.Map[String,String]("firstkeyisforb"->"firstkeyisforb")
	
	
//first pass of the training set to evaluate inverse topic frequency
	val topicfreq = collection.mutable.Map[String,Set[String]]() 
	var cntt=0;
   	val topicCounts = scala.collection.mutable.Map[String, Int]()
   		val documentfreq = collection.mutable.Map[String,Int]() 

	while(firstiter.hasNext){
	    	val doc = firstiter.next
	    	if(doc.topics.size!=0&&doc.tokens.size!=0){
	    		val tokenized=stopper.filter(doc.tokens.map(_.toLowerCase))
	    		stemmed++=tokenized.map(s=>s->stemmed.getOrElse(s,p.stem(s)))
	    		topicfreq ++= tokenized.map(stemmed.get(_).get).map(t => t-> (topicfreq.getOrElse(t,Set.empty) | doc.topics))
	    		documentfreq ++= tokenized.map(stemmed.get(_).get).map(t => t-> (documentfreq.getOrElse(t,0) +1))

	    		cntt+=1
	    		topicCounts ++= doc.topics.map(c => (c -> (1 + topicCounts.getOrElse(c, 0))))
	    		if(cntt%1000==0)
	    		println(cntt)
	    	}
	    }
	val topics=topicCounts.keys
	val iTopicFreq=topicfreq.mapValues(x=>if (x!=0) log2(topics.size) - log2(x.size) else 0)
	
	
//second pass of the training set to evaluate the document representation
    val docrep=scala.collection.mutable.ArrayBuffer.empty[(Set[String],scala.collection.immutable.Map[String, Double])]
  	var count = 0;
 	while(iter.hasNext){
	   	val doc = iter.next
	   	if(doc.topics.size!=0&&doc.tokens.size!=0){
	   		val tokenized=stopper.filter(doc.tokens.map(_.toLowerCase))
	    	//stemmed++=tokenized.map(s=>s->stemmed.getOrElse(s,p.stem(s)))
	   		count += 1
	   		//each document is represented by its topics and its most important words
	   		docrep+=((doc.topics,((tokenized.map(stemmed.get(_).get).groupBy(identity).map{case (k,l) => (k->l.length.toDouble)}).toList.sortBy(x=>(log2(1.0+x._2/tokenized.size)*iTopicFreq.get(x._1).get)).takeRight(200):+("firstkeyisforb"->1.0)).toMap))
	   		if(count%1000==0)
	   			println("pre " + count)
	    }
	}
	//probability of a topic	
	val pc=topicCounts.mapValues(x=>x.toDouble/count)	
	
	val feature=new FeatureSelection(documentfreq,topicCounts,stemmed.values.toSet.filter(_!="firstkeyisforb"),topics.toSet,docrep.map{case(s,m)=>(s,m.filter(_._1!="firstkeyisforb"))})
	  	val featurefilter=topicCounts.map{case(k,v)=>k->feature.selectBest(k)}

	
	
	
	//thetaM is a map of theta maps, one for each topic	
	var thetaM=pc.mapValues(x=>scala.collection.mutable.Map[String,Double]())
	
	//probability function of the document being positive given theta
	def pfun(theta:scala.collection.Map[String, Double],
	     xd:scala.collection.Map[String, Double]):Double={
				1.0/(1.0+exp(-vec(xd,theta)))}
	  	  	
	  	
	//update of theta for the topic x  	
	def update(x:String,topics:Set[String],t:Double,theta:scala.collection.mutable.Map[String,Double],xd:scala.collection.Map[String, Double])={
  	  val prob=pfun(theta,xd)
  	  if (topics(x)){
	  		 sum(theta,(xd.map(v=>(v._1->(1.0*v._2/t*(1-pc.get(x).get)*(1-prob))))))}
	  else {	
	  		 sum(theta, (xd.map(v=>(v._1->(-1.0*v._2/t*(pc.get(x).get)*(prob))))))}
	  }
	  	
	
	//learning  	
		val random : Stream[Int] = Stream.continually(Math.abs(Random.nextInt) % docrep.size)
		var t=0
		for(n<-random.take(800000)){
	  		t+=1	  		 
	  		topics.foreach(top=> {thetaM+=(top->update(top,docrep(n)._1,sqrt(t),thetaM.get(top).get,docrep(n)._2.filter(x=>featurefilter.get(top).get(x._1))))})
	    	if(t%1000==0)
	  		println("learning: "+t)
	  	}
	 	
	
	//labelled test
		
  	val pathTL = folderpath+"data/test-with-labels"
	var iterTL = new ReutersCorpusIterator(pathTL)
  	val res=scala.collection.mutable.Buffer[(Int,String)]()
  	var ccc=0
  	var precision=0.0
  	var recall=0.0
  	var F1=0.0
	  	
  	while(iterTL.hasNext){
  		ccc+=1
  		if(ccc%1000==0)
  		println("labelled test: "+ccc)
  		var truePositive=0
	  	var falsePositive=0
	   	val doc = iterTL.next
	    val tokenized=stopper.filter(doc.tokens.map(_.toLowerCase))
	    stemmed++=tokenized.map(s=>s->stemmed.getOrElse(s,p.stem(s)))
	    val input=tokenized.flatMap(x=>stemmed.get(x))

//as before: just the most important words of the document are selected
	    val tf= (input.groupBy(identity).map{case (k,l) => (k->l.length.toDouble)}.toList.sortBy(x=>(log2(1.0+x._2/tokenized.size)*iTopicFreq.getOrElse(x._1,0.0))).takeRight(50):+ ("firstkeyisforb"->1.0)).toMap
	    
	    topics.foreach(x=> if(pfun(thetaM.get(x).get,tf)>0.5){res+=((doc.ID,x));if(doc.topics(x)) truePositive+=1 else falsePositive+=1})
	    if(truePositive!=0&&falsePositive!=0){
	    		val pre=truePositive.toDouble/(truePositive+falsePositive)
	    		val rec=truePositive.toDouble/doc.topics.size
	    		precision+=pre
	    		recall+=rec
	    		if(pre+rec!=0){
	    			F1+=2*pre*rec/(pre+rec)
	    		}
	    }
	}
	 
	 val writer=new PrintWriter(folderpath+"classify-[francesco]-[locatello]-[l]-[lr].txt", "UTF-8")
	 writer.println(precision/ccc+" "+recall/ccc+" "+F1/ccc)
	 val s=res.groupBy(_._1).mapValues(x=>x.map(_._2)).foreach(x=>writer.println(x._1+" "+x._2.mkString(" ")))
	 writer.close()
	 
//unlabelled data	 
	 val pathTU = folderpath+"data/test-without-labels"
	 var iterTU = new ReutersCorpusIterator(pathTU)
	 val resU=scala.collection.mutable.Buffer[(Int,String)]()
	 var unlabeled=0
	 while(iterTU.hasNext){
	  	unlabeled+=1
	  	if(unlabeled%1000==0)
	  		println("unlabeled: "+unlabeled)
	  	val doc = iterTU.next
	    val tokenized=stopper.filter(doc.tokens.map(_.toLowerCase))
	    stemmed++=tokenized.map(s=>s->stemmed.getOrElse(s,p.stem(s)))
	    val input=tokenized.flatMap(x=>stemmed.get(x))
	    val tf= (input.groupBy(identity).map{case (k,l) => (k->l.length.toDouble)}.toList.sortBy(x=>(log2(1.0+x._2/tokenized.size)*iTopicFreq.getOrElse(x._1,0.0))).takeRight(200):+ ("firstkeyisforb"->1.0)).toMap
	    topics.foreach(x=> if(pfun(thetaM.get(x).get,tf)>0.5){res+=((doc.ID,x))})
		}
	 val writer2=new PrintWriter(folderpath+"classify-[francesco]-[locatello]-[u]-[lr].run", "UTF-8")
	 val sU=resU.groupBy(_._1).mapValues(x=>x.map(_._2)).foreach(x=>writer.println(x._1+" "+x._2.mkString(" ")))
	 writer2.close()
   }
}