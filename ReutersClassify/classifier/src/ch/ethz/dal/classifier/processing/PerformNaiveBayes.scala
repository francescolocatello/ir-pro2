package ch.ethz.dal.classifier.processing
import java.io.File
import java.io.FileNotFoundException
import java.util.zip.ZipEntry
import java.util.zip.ZipFile
import scala.collection.JavaConversions.enumerationAsScalaIterator
import scala.collection.mutable.Queue
import scala.util.Success
import scala.util.Try
import Array._
import scala.math._
import java.io.PrintWriter

object PerformNaiveBayes {
  //def eval={
//	val nb=(new NaiveBayes).get() 
	//val pc=nb._1
	//val pwc=nb._2
	//val denominator=nb._3
	//val topics=nb._4
	//val stemmed=nb._5
	 // val path = "/Users/francescolocatello/Documents/ETH/Corsi/Information Retrieval/Project2/data/test-with-labels"
	 // 	val iter = new ReutersCorpusIterator(path)
	 //   val p=new PorterStemmer
	 //   val stopper=new StopWords
	    //val stemmed= collection.mutable.Map[String,String]()
	  //  val writer=new PrintWriter("/Users/francescolocatello/Documents/ETH/Corsi/Information Retrieval/Project2/naive.txt", "UTF-8")
	   // def log2 (x: Double) :Double = log10(x) / log10(2.0)
	  //	var count = 0 
	  	//writer.println(denominator)
	  	var truePositive=0
	  	var falsePositive=0
	  	var totPositive=0
	  	var totTopic=0
	  	val res=scala.collection.mutable.Buffer[(Int,String)]()
	  	//val res=scala.collection.mutable.Buffer[(Int,scala.collection.immutable.Set[String])]()
	  	var avg=0.0
	  	var cnt=0
	//	while(iter.hasNext){
	//		count+=1
	//    	val doc = iter.next
	//    	val tokenized=stopper.filter(doc.tokens)
	 //   	nb._5++=tokenized.map(s=>s->nb._5.getOrElse(s,p.stem(s)))
	 //   	totTopic+=doc.topics.size
	 //   	val input=tokenized.flatMap(x=>nb._5.get(x))
	 //   	val tf= input.groupBy(identity).mapValues(l => l.length)
	 //   	println (count)
	 //   	if (doc.topics.size!=0&&tf.size!=0) { 
	    	//  cnt+=1
	    	  
	    	
	    	  
	//		val e=nb._4.map(x=>(x,log(nb._1.getOrElse(x,0.0)) +
	 //   	    (tf.map{case (w,tef)=>tef*log(nb._2.getOrElse((w,x),1/nb._3.getOrElse(x,0))).toDouble}.reduce(_+_)))).maxBy(_._2)
	    	//println(e)
			    
			    
			    
			    //val e=doc.topics.map(x=>(x,log(nb._1.getOrElse(x,0).toDouble) +
	    	//    (tf.map{case (w,tef)=>tef*log(nb._2.getOrElse((w,x),1/nb._3.getOrElse(x,0))).toDouble}.reduce(_+_)))).filter(x=>x._2>0.5).map(x=>x._1)
	    	//avg+=e._2
	    	//println(e._2)
	    	//res+=((doc.ID,e))
	    	//println(e.size)
	    	//res+=((doc.ID,e._1))
//	    	totPositive+=doc.topics.size
	    	//if (doc.topics( e._1)) truePositive+=1 else falsePositive+=1 
	    	//truePositive+=(doc.topics & e).size
	    	//falsePositive+=(e -- doc.topics).size
	    	//println (count+" "+cnt)
//	    	}	
//		}
	//println (avg+" "+count)
	//println("media "+avg/count)

	  val precision=truePositive.toDouble/(truePositive+falsePositive)
	  val recall=truePositive.toDouble/totTopic
	  val F1=2*precision*recall/(precision+recall)
	  
	 // writer.println(precision+" "+recall+" "+F1)
	 
	 //res.foreach(x=>writer.println(x._1+" "+x._2))
//	  writer.close()
//  }
}