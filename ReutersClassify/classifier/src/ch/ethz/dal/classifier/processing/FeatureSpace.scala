package ch.ethz.dal.classifier.processing
import java.io.PrintWriter


object FeatureSpace extends App{
    val path = "/Users/francescolocatello/Documents/ETH/Corsi/Information Retrieval/Project2/data/train"
	val iter = new ReutersCorpusIterator(path)
    val stemmed= collection.mutable.Map[String,String]()
	val p=new PorterStemmer
	val stopper=new StopWords
	var cnt=0
	println(cnt)
	while(iter.hasNext){
			cnt+=1
			println(cnt)
	    	val doc = iter.next
	    	val tokenized=stopper.filter(doc.tokens)
	    	stemmed++=tokenized.map(s=>s->stemmed.getOrElse(s,p.stem(s)))
	    	//space ++= tokenized./*flatMap(x=>stemmed.get(x)).*/map(c => (c -> (1 + space.getOrElse(c, 0))))
	    }
        val writer = new PrintWriter("/Users/francescolocatello/Documents/ETH/Corsi/Information Retrieval/Project2/FeatureSpaceTf.txt", "UTF-8");

		writer.println(stemmed.values)
		writer.close()
  
  
}