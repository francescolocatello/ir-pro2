package ch.ethz.dal.classifier.processing

object Main extends App {
  val path=	"/Users/francescolocatello/Documents/ETH/Corsi/Information Retrieval/Project2/"
val iterationSVM=200000
val iterationLR=300000
val dimensionThetaSVM=100
val dimensionThetaLR=1000

	
	//NaiveBayes.perform(path)
    SVM.computeSVM(path,iterationSVM,dimensionThetaSVM)
    //LogisticRegression.logisticRegression(path, iterationLR,dimensionThetaLR)
}