package ch.ethz.dal.classifier.processing
import java.io.File
import java.io.FileNotFoundException
import java.util.zip.ZipEntry
import java.util.zip.ZipFile
import scala.collection.JavaConversions.enumerationAsScalaIterator
import scala.collection.mutable.Queue
import scala.util.Success
import scala.util.Try
import Array._
import scala.math._
import java.io.PrintWriter
import scala.util.Random



 
object SVM {
  
  //vec: perform dot product between two maps
   def vec (m: scala.collection.Map[String,Double],other: scala.collection.Map[String,Double]) : Double ={
    m.map{case (k,v) => v*other.getOrElse(k,0.0)}.sum
     }
   //sum: sum two maps 
   def sum(a:scala.collection.mutable.Map[String,Double],b:scala.collection.Map[String,Double]):scala.collection.mutable.Map[String,Double]={ 
		   b.foreach{case (k,v)=> a+= ((k, v+ a.getOrElse(k, 0.0)))}
		   a}

   def computeSVM(pathfolder:String,ITERATION:Int,dimension:Int)={
	val path = pathfolder+"data/train"
	val iter = new ReutersCorpusIterator(path)
	val firstiter=new ReutersCorpusIterator(path)

	val p=new PorterStemmer
	val stopper=new StopWords
	//"firstkeyisforb" is a fake key that I use to put b inside theta
	val stemmed= collection.mutable.Map[String,String]("firstkeyisforb"->"firstkeyisforb")
	def log2 (x: Double) :Double = log10(x) / log10(2.0)    
	val topicCounts = scala.collection.mutable.Map[String, Int]()
	val topicWords=scala.collection.mutable.Map[String, Set[String]]()
	val documentfreq = collection.mutable.Map[String,Int]() 
	
 	var count = 0;
	//compute the representation of the document.
	val docrep=scala.collection.mutable.ArrayBuffer.empty[(Set[String],scala.collection.immutable.Map[String, Double])]

	while(iter.hasNext){
	    	val doc = iter.next
	    	if(doc.topics.size!=0&&doc.tokens.size!=0){
	    		val tokenized=stopper.filter(doc.tokens.map(_.toLowerCase)):+ "firstkeyisforb"
	    		stemmed++=tokenized.map(s=>s->stemmed.getOrElse(s,p.stem(s)))
	    		count += 1
	    		val docum=tokenized.map(stemmed.get(_).get).groupBy(identity).map{case (k,l) => (k->l.length.toDouble)} 		    
	    		docrep+=((doc.topics,docum))
	    		doc.topics.foreach(x=>topicWords+=(x->(docum.keys.toSet|topicWords.getOrElse(x,Set.empty))))
	    		topicCounts ++= doc.topics.map(c => (c -> (1 + topicCounts.getOrElse(c, 0))))
	    		documentfreq ++= tokenized.map(stemmed.get(_).get).map(t => t-> (documentfreq.getOrElse(t,0) +1))
	    		if(count%1000==0)
	    		println("pre " + count)
	    	}
	    }
	   	val topics=topicCounts.keys

	   	
	   val feature=new FeatureSelection(dimension,documentfreq,topicCounts,stemmed.values.toSet.filter(_!="firstkeyisforb"),topics.toSet,docrep.map{case(s,m)=>(s,m.filter(_._1!="firstkeyisforb"))})	   	
	   	
	   	val pc=topicCounts.mapValues(x=>x.toDouble/count)
	  	val max=pc.values.max
	  	val featurefilter=topicCounts.map{case(k,v)=>k->feature.selectBest(k)}
		val trasdocrep=docrep
	  	var thetaM=pc.mapValues(x=>scala.collection.mutable.Map[String,Double](/*"firstkeyisforb"->0*/))
	  	
	  	def pfun(theta:scala.collection.Map[String, Double],
	  	    xd:scala.collection.Map[String, Double]):Double={
	  	  1.0/(1.0+exp(-vec(xd,theta)))
	  	}
	  	
	  	var t=0.0
	  	
	  	
	  	
	  	val lambda=0.05
	  	
	  	def y(x:String,topics:Set[String])={
	  	  	if(topics(x)) 1 else -1
	  	}
	  	
	  	def update(x:String,topics:Set[String],t:Double,theta:scala.collection.mutable.Map[String,Double],xd:scala.collection.Map[String, Double])={
	  	  theta.transform{case (k,v) =>(if (k!="firstkeyisforb") v*(1-1.0/t) else v)}
	  	  val yy=y(x,topics)
	  	  if(yy>0){
	  	  if(yy*vec(xd,theta)<1){
	  		    sum(theta,xd.mapValues(e=>(3.0)/((1-max+(pc(x)))*(lambda*t))*yy*e))
	  	  }
	  	  else theta
	  	}
	  	  else{
	  	    if(yy*vec(xd,theta)<1){
	  		    sum(theta,xd.mapValues(e=>(1.0)/(lambda*t)*yy*e))
	  	  }
	  	  else theta
	  	    
	  	  }
	  	}
  	
	  	val writer=new PrintWriter(pathfolder+"classify-[francesco]-[locatello]-[l]-[sv].run", "UTF-8")
		val random : Stream[Int] = Stream.continually(Math.abs(Random.nextInt) % trasdocrep.size)
	  
	  	for(n<-random.take(ITERATION)){
	  		t+=1	  		 
	  		topics.foreach(top=> {thetaM+=(top->update(top,docrep(n)._1,sqrt(t),thetaM.get(top).get,docrep(n)._2.filter(x=>featurefilter.get(top).get(x._1))))})
	  		println("train: "+t)
	  	}
	  		
	  	
	  	val pathTL = pathfolder+"data/test-with-labels"
		var iterTL = new ReutersCorpusIterator(pathTL)
	  	val res=scala.collection.mutable.Buffer[(Int,String)]()
	  	
	  	
	  	var ccc=0
	  	var precision=0.0
	  	var recall=0.0
	  	var F1=0.0
	  	
	  	while(iterTL.hasNext&&ccc<0){
	  	  ccc+=1
	  	  var truePositive=0
	      var falsePositive=0
	  	  println("test "+ccc)
	  		 
	  	   	val doc = iterTL.next
	    	val tokenized=stopper.filter(doc.tokens.map(_.toLowerCase)):+ "firstkeyisforb"
	    	stemmed++=tokenized.map(s=>s->stemmed.getOrElse(s,p.stem(s)))
	    	val input=tokenized.flatMap(x=>stemmed.get(x))
	    	val tf= (input.groupBy(identity).map{case (k,l) => (k->l.length.toDouble)})
	    	
	    	res+=((doc.ID,""))
	    	topics.foreach(x=> if(vec(tf,thetaM.get(x).get)>0){res+=((doc.ID,x));if(doc.topics(x)) truePositive+=1 else falsePositive+=1})
	    	
	    	
	    	if(truePositive!=0&&falsePositive!=0){
	    	val pre=truePositive.toDouble/(truePositive+falsePositive)
	    	val rec=truePositive.toDouble/doc.topics.size
	    	
	    	precision+=pre
	    	recall+=rec
	    	
	    	if(pre+rec!=0){
	    		F1+=2*pre*rec/(pre+rec)

	    		}
	    	}
	  	}
	 writer.println(precision/ccc+" "+recall/ccc+" "+F1/ccc)
	 val s=res.groupBy(_._1).mapValues(x=>x.map(_._2)).foreach(x=>writer.println(x._1+" "+x._2.mkString(" ")))

	 writer.close()
//unlabelled data	 
	 val pathTU = pathfolder+"data/test-without-labels"
	 var iterTU = new ReutersCorpusIterator(pathTU)
	 val resU=scala.collection.mutable.Buffer[(Int,String)]()
	 var unlabelled=0
	 while(iterTU.hasNext){
	  	unlabelled+=1
	  	if(unlabelled%1000==0)
	  		println("unlabeled: "+unlabelled)
	  	val doc = iterTU.next
	  	resU+=((doc.ID,""))

	    val tokenized=stopper.filter(doc.tokens.map(_.toLowerCase)):+ "firstkeyisforb"
	    stemmed++=tokenized.map(s=>s->stemmed.getOrElse(s,p.stem(s)))
	    val input=tokenized.flatMap(x=>stemmed.get(x))
	    val tf= (input.groupBy(identity).map{case (k,l) => (k->l.length.toDouble)})
	    topics.foreach(x=> if(vec(tf,thetaM.get(x).get)>0){resU+=((doc.ID,x))})
		}
	 val writer2=new PrintWriter(pathfolder+"classify-[francesco]-[locatello]-[u]-[sv].run", "UTF-8")
	 resU.groupBy(_._1).mapValues(x=>x.map(_._2)).foreach(x=>writer2.println(x._1+" "+x._2.mkString(" ")))
	 writer2.close()

}
}