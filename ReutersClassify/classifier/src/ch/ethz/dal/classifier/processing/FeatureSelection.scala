package ch.ethz.dal.classifier.processing
import Array._
import math._

class FeatureSelection (
    dimension:Int,
    documentFreq:collection.mutable.Map[String,Int],
    topicCounts:scala.collection.mutable.Map[String, Int],
    stemmed:Set[String], topics:Set[String], 
    docrep:scala.collection.mutable.ArrayBuffer[(Set[String],scala.collection.immutable.Map[String, Double])])
{
	def log2 (x: Double) :Double = log10(x) / log10(2.0)

    val feature=scala.collection.mutable.Map[String,Set[String]]()
    var N11 = Array.fill[Double](topics.size,stemmed.size)(0.0)
    val topicToInt=topics.toList.zipWithIndex.toMap
    val wordsToInt=stemmed.toList.zipWithIndex.toMap 
    val N=docrep.size
    println("building matrix...")
    docrep.foreach{case (s,m)=>s.foreach(t=>m.foreach(w=>{N11(topicToInt.get(t).get)(wordsToInt.get(w._1).get)+=1}))}
    println("done")
    val docFreq=collection.mutable.Map[String,Int]()

	def N01(t:String,w:String)={
	  topicCounts.get(t).get-N11(topicToInt(t))(wordsToInt(w))
	}
     def N10(t:String,w:String)={
	  documentFreq.get(w).get-N11(topicToInt(t))(wordsToInt(w))
	}
     def N00(t:String,w:String)={
	  N-N01(t,w)-N10(t,w)-N11(topicToInt.get(t).get)(wordsToInt.get(w).get)
	}
     def N0dot(t:String,w:String)={
       N01(t,w)+N00(t,w)
     }
     def N1dot(t:String,w:String)={
       N11(topicToInt.get(t).get)(wordsToInt.get(w).get)+N10(t,w)}
     def Ndot0(t:String,w:String)={N00(t,w)+N10(t,w)}
     def Ndot1(t:String,w:String)={
       N11(topicToInt.get(t).get)(wordsToInt.get(w).get)+N01(t,w)}

     
	
    def information(w:String,t:String):Double={
    	val tnum=topicToInt.get(t).get
    	val wnum=wordsToInt.get(w).get
    	val M=N11(tnum)(wnum)
    	if(M!=0&&N01(t,w)!=0&&N10(t,w)!=0&&N00(t,w)!=0){
    	    	M/N*log2(N*M/(N1dot(t,w)*Ndot1(t,w)))+
    		N01(t,w)/N*log2(N*N01(t,w)/(N0dot(t,w)*Ndot1(t,w)))+N10(t,w)/N*log2(N*N10(t,w)/(N1dot(t,w)*Ndot0(t,w)))+N00(t,w)/N*log2(N*N00(t,w)/(N0dot(t,w)*Ndot0(t,w)))

    	}
    	else 0
    	}
    
    var cont=0
    def selectBest(topic:String)={
      cont+=1
      println(cont)
     (stemmed.toList.sortBy(information(_,topic)).takeRight(dimension):+"firstkeyisforb").toSet
    }
  

}