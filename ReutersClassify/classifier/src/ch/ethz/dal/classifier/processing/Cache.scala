package ch.ethz.dal.classifier.processing



import java.io._

object Cacher {
  def write(x : Any, file: String) {
    printf("Writing %s...\n", file)
    val output = new ObjectOutputStream(new FileOutputStream("cache/" + file))
    output.writeObject(x)
    output.close()
    printf("Done.\n")
  }

  def read[A](file: String): A = {
    printf("Reading %s...\n", file)
    val input = new ObjectInputStream(new FileInputStream("cache/" + file))
    val obj = input.readObject()
    input.close()
    printf("Done.\n")
    obj.asInstanceOf[A]
  }
}
