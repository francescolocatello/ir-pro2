package ch.ethz.dal.classifier.processing

object Tokenizer {
  def tokenize (text: String) : List[String] =
    text.split("[ --&%=\'\".,;:/()_?!\\\t\n\r\f1234567890]+").toList
}